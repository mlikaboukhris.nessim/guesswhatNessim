<?php

namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
    const ORDER_COLOR = ['Coeur' => 4, 'Carreau' => 3, 'Pique' => 2, 'Trèfle' => 1];
    const ORDER_NAMES = [
        '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6,
        '7' => 7, '8' => 8, '9' => 9, '10' => 10,
        'Valet' => 11, 'Reine' => 12, 'Roi' => 13, 'As' => 14
    ];
    /**
     * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
     */
    private $name;

    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): void
    {
        $this->color = $color;
    }


    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $o1 Card
     * @param $o2 Card
     * @return int
     * <ul>
     *  <li> zéro si $o1 et $o2 sont considérés comme égaux </li>
     *  <li> -1 si $o1 est considéré inférieur à $o2</li>
     * <li> +1 si $o1 est considéré supérieur à $o2</li>
     * </ul>
     *
     */
    public static function cmp(Card $o1, Card $o2): int
    {
        $o1Name = strtolower($o1->name);
        $o2Name = strtolower($o2->name);
        $o1Color = strtolower($o1->color);
        $o2Color = strtolower($o2->color);

        //Si les couleurs et noms sont pareils
        if ($o1Name == $o2Name && $o1Color == $o2Color) {
            return 0;
            //Si le nombre est inférieur mais la couleur est la même
        } elseif ($o1Name < $o2Name && $o1Color == $o2Color) {
            return -1;
            //Si le nombre est supèrieur mais la couleur est la même
        } elseif ($o1Name > $o2Name && $o1Color == $o2Color) {
            return +1;
            //Si les nombres sont les mêmes mais la couleur est inférieure
        } elseif ($o1Name == $o2Name && $o1Color < $o2Color) {
            return -11;
            //Si les nombres sont les mêmes mais la couleur est suprieure
        } elseif ($o1Name == $o2Name && $o1Color < $o2Color) {
            return +11;
            //Si le nombre est supèrieur et la couleur est supèrieure
        } elseif ($o1Name > $o2Name && $o1Color > $o2Color) {
            return +01;
            //Si le nombre est infèrieur et la couleur est inférieure
        } elseif ($o1Name < $o2Name && $o1Color < $o2Color) {
            return -01;
        } else {
            return 99;
        }
    }
}